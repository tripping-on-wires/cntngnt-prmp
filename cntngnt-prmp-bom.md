Parts are necessarily variable but here's a rough guide. Check the published 'recipes' for more specific values.

Resistors
----------

| Part Name | Type | Value | Count |
| :--------| :------| :-----| :-----|
| Gate Resistor | Carbon film 1/4W | ~3M (1M-10M) | 1
| Source Resistor | Carbon film 1/4W | 240R-3.9K | 1
| Drain Resistor | Carbon film 1/4W | 2K-100K | 1
| Output Ground Ref Resistor | Carbon film 1/4W | varies | 1
| | | | __4__


Capacitors
----------

| Part Name | Type | Value | Count |
| :--------| :------| :-----| :-----|
| Power Capacitor | Electrolytic Capacitor| 10µF | 1
| DC Capacitor | Electrolytic Capacitor | 4.7µF | 1
| | | | __2__
_Note: 25V should be fine for most applications but if you adapt for e.g. phantom power you might want capacitors rated for 50V or more._

Transistors
----------

|  Part Name| Type | Value | Count |
| :--------| :------| :-----| :-----|
| JFET | JFET | J201, MPF102, 2N3819, etc. | 1
| | | | __1__
_Note: Alternatives could be 2N5457, BF245C, 2N5486, 2N5458, NTE457, NTE469, J113, 2N4416 (mostly untested so check datasheets for pin order etc.)_

External Connections
----------

|  Part Name| Type | Value | Count |
| :--------| :------| :-----| :-----|
| Input Jack | Jack socket | 3.5mm or 1/4" | 1
| Output Jack | Jack socket | 3.5mm or 1/4" | 1
| Power Obelisk | Battery snap | 9v PP3 connector | 1
| | | | __3__
